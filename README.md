# grav livingutopia skeleton

## setup
`$ git clone --recursive https://gitlab.com/livingutopia/grav-skeleton-livingutopia.git`

`$ cd grav-skeleton-livingutopia`

`$ ./bin/grav install`


## running
`$ php -S 127.0.0.1:1111 system/router.php`
