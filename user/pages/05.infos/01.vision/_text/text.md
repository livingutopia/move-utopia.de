---
title: Visionstext
---


# MOVE - Die Vision

### Das Move ist…

<ul style="list-style: none">
  <li> … ein Raum um Abgrenzungen und Vorurteile zwischen verschiedenen Bewegungen und deren Ansätze sich zu organisieren und Wandel zu gestalten aufzulösen und sich möglichst offen begegnen zu können</li>
  <li>… ein Raum an dem unterschiedliche Bewegungen gemeinsame Strategien entwickeln können um sich gegenseitig bei ihren Kämpfen zu unterstützen</li>
  <li>… ein Mitmachraum bei dem es Aufmerksamkeit und Wertschätzung für verschiedene Tätigkeiten gibt (Care-Tätigkeiten, Aktionen, …)</li>
  <li>… ein Ort mit vielen Workshops, Podiumsdiskussionen, und ähnlichem um Wissen zu teilen</li>
  <li>… ein Ort der es ermöglicht Erfahrungen damit zu machen, wie es ist die Werte von MOVE zu leben</li>
  <li>… ein Ort an dem es Strukturen dafür gibt, dass du nicht alleine über das MOVE laufen musst, sondern der dir ermöglicht Menschen kennenzulernen und Freundschaften zu knüpfen</li>
  <li>… ein Ort an dem du aus deiner Komfortzone rausgehen und andere Lebensrealitäten kennenlernen kannst</li>
  <li>… ein Startschuss für eine gemeinsame Handlungsbasis aller Menschen, die an bestehenden Verhältnissen etwas verändern wollen / „das gute Leben für alle“ wollen</li>
  <li>… ein Raum in dem wir alle die Verantwortung tragen, dass es zu einem MOVE wird, dass unserer Utopie entspricht</li>
  <li>… ein Ort an dem alle ihre Fähigkeiten und Bedürfnisse einbringen können</li>
</ul>



Das MOVE soll nicht ein einziges Event bleiben, sondern einen Knotenpunkt darstellen von dem aus verschiedene Bewegungen ihre Netze weiterspinnen, wachsen und sich weiter bewegen können.

Wir träumen davon, dass sich auf dem MOVE einzelne Menschen zu Gruppen zusammenfinden, die längerfristig gemeinsam wirken wollen.
Dabei will das MOVE einen Rahmen und eine Struktur bieten um Wissen weiter zu vermitteln und Austausch zu fördern.
