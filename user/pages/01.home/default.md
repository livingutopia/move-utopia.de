---
title: Home
body_classes: 'title-center title-h1h2'
---

# Willkommen findibus

findus ist wunderbar
!!! Glückwunsch, es läuft

### Links
- Viel Wissen über Grav: [learn.getgrav.org](https://learn.getgrav.org)
- Das Adminmenü: [/admin](/admin)
- Eine Übersicht der Komponenten für modulare Seiten: [hier](/modular)
